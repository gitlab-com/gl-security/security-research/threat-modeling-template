## Threat Model

We're following the [Evidence Driven PASTA Threat Modeling approach](https://about.gitlab.com/handbook/security/threat_modeling/#evidence-driven-threat-model)
as [outlined in the handbook](https://about.gitlab.com/handbook/security/threat_modeling/)

For the AppSec specifics please refer to the [threat modeling workflow handbook page](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/threat-modeling.html).

<!-- 
How to use this template:
 
Each section has a few steps with checklists and a cut and paste snippet.
 
The snippet should be pasted in an issue comment and therein the steps
should be documented.
-->

### Setup

- [ ] Link the original issue / epic back to this issue and put a note about this threat model in the original issue / epic.
- [ ] Please provide a comment to this issue with a short reasoning why the linked issue has been labeled ~"threat model::needed".

Cut and paste snippet:

```
## Threat model reasoning

Describe briefly why the threat model is needed.
```

- [ ] Outline the to be covered scope for the threat model in an issue comment.

Cut and paste snippet:

```
## Scope

Describe the scope of the threat model.
```


### Application decomposition

Describe the review item in terms of:

- [ ] What is the use case / purpose of the to be reviewed item?
- [ ] Where are the entry points for external/attacker controlled data?
- [ ] Are there different trust levels / domains? Are there trust boundaries which result from the architecture?
- [ ] Based upon previous security issues, create a list of areas or the types of vulnerabilities / security misconfigurations that are likely to occur.
- [ ] If possible / needed provide a Data Flow Diagram which breaks down the review item highlighting the above points: trust levels and external inputs.
- [ ] If applicable, link a well known standard, best practice, or reference implementation to compare with this implementation.


Cut and paste snippet:
```
## Application decomposition

### Use case

### External entrypoints

### Trust levels

### Data flow diagram

### Previous security issues

### Known references and best practices
```
- [ ] Ping the development counterpart(s) to review / comment on the above points.
- [ ] Iterate to integrate the feedback from the review.


### Threat analysis

- [ ] Take an [attackers perspective](https://about.gitlab.com/handbook/security/threat_modeling/howto.html#finding-the-threats) and assume worst cases to define the threats.
  - [ ] Try to order by most likely and impactful threats first.
- [ ] Ping the development counterpart(s) to review / comment on the above identified threats.
  - [ ] Map the threats back to the feature. Where are they considered / mitigated? If so, linking code is recommended.
  - [ ] Describe likelihood and impact of identified potential threats
- [ ] Iterate to integrate the feedback from the review.

Cut and paste snippet:

```
## Threat analysis

Provide a short conclusion and summary of threats

### Identified threats


#### Overview

| Threat                 | Comments     | Severity        | Likelihood      | Test                           |
|------------------------|--------------|-----------------|-----------------|--------------------------------|
|       Some threat      |      TDB     | High/Medium/Low | High/Medium/Low | ✅/❌/❓  Link to issue/code   |


```

### Vulnerability and weakness analysis

Validated threats should be documented in a follow up issue which is linked in the overview table.
When documented threats are either deemed not applicable, their risk is accepted or a follow up issue
is created the threat model can be concluded in the next step. As features change, additional threat
models might be necessary. In that case a new issue should be created. The new issue should be linked
to the previous threat modeling issues.

### Concluding the threat model

- [ ] To finalize the threat model the individual threat model parts should be collected in a single markdown file and merged into this repository.
The merge request should be peer reviewed by an AppSec team member. Once merged the issue can be closed.
