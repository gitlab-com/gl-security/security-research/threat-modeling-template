## Threat model

- [ ] Draw [a diagram](https://about.gitlab.com/handbook/security/threat_modeling/howto.html#drawing-diagrams) of your feature and point out:
  - [ ] Where does untrusted input come from?
  - [ ] Where are zones of different [trust levels and where are the trust boundaries](https://about.gitlab.com/handbook/security/threat_modeling/howto.html#trust-and-boundaries) between those?
- [ ] Take an [attackers perspective](https://about.gitlab.com/handbook/security/threat_modeling/howto.html#finding-the-threats) and assume worst cases to define the threats.
  - [ ] Try to order by most likely and impactful threats first.
- [ ] Document the threats and map them back to your feature. Where are they considered / mitigated?


### Data Flow Diagram

```mermaid
graph TD
    A[Actor] -->|Does something| B(Thing) --> |Interacts with| C(Another thing)
    
    subgraph u[Untrusted]
       A
    end
    
    subgraph l[Low trust]
      B
    end
    
    subgraph h[Internal, fully trusted]
      C
    end
```

### Identified Threats

| Threat                 | Comments     | Severity        | Test                              |
| ---------------------- | -------------|---------------- | ----------------------------------|
|       Some threat      |      TDB     | High/Medium/Low |    ✅/❌/❓  Link to issue/code   |
