## Name of Project - Threat Model Eval

<!--
Replace "Name of Project" above
  -->

### Description of Project
<!--
Include a brief description of the project. This can be only one sentence if one sentence covers it. Think of this as the "goal" of the project.
  -->

---
<!--  
NOTE: Each Stage below includes some starter questions, but feel free to use your own or simply sum things up. Leave the comments if you wish, as the may help you during the process.

The questions are intended to help start using the framework. Add and delete questions as needed.
  -->
## Stage I - Define objectives
<!--
 * Business objectives. State the business objective of the project. This can be the purpose of the code, the change to the infrastructure, the goal of the marketing campaign, whatever the objective is.
 * Security, Compliance, and Legal requirements. These are both guidelines and limits in a broad sense. For example, the licensing for a proposed third party application should be in order, the privacy policy of the new SaaS application being considered meets muster, or the new code does not require lowering security standards.
 * Business impact analysis. This should include impact to mission and business processes, recovery processes, budget impact, and system resource requirements.
 * Operational impact. This should include impact to existing processes already in use by operational personnel. For example, extra logging could be introduced that might impact interpretation of system events, increase the number of steps for future changes, or alter documented procedures for advanced troubleshooting.
  -->
Describe the overall project and the objectives.

- Are there any special requirements that must be meant (Security, Compliance, and/or Legal):

- Business impact as a result of this project, if the part of a larger project/objective, note that here:

- Operational impact to any additional systems, processes, personnel, etc:

## Stage II - Define technical scope
<!--
* Boundaries of technical environment. These are the boundaries related to the project itself. This should include the type of data (by classification) that will be touched with this project.
* Infrastructure, application, software dependencies. All of these need to be documented, including the level of impact. For example, if the project requires an upgrade to a subsystem's library and the subsystem has access read-only access to RED data, this needs to be documented as it could impact other users of the subsystem as well as introduce a potential attack vector to RED data.
  -->
This creates the boundaries for the project.

- What part of the infrastructure will my project reside in?

- What dependencies does my project rely on? (code such as libraries, or otherwise)

- What permissions does my project require?

- What components does my project talk to, for authorization or for data reading/writing?

- Will this project create new data, and what is the classification of that data?

## Stage III - Application Decomposition
<!--
 * Identify use cases, define application entry points and trust levels even if they are to remain unchanged as a result of the project.
 * Identify actors, assets, services, roles, and data sources.
 * Document data movement (in motion, at rest) via Data Flow Diagramming. Show trust boundaries, including existing as well as proposed changes.
 * Document all data touched and its classification.
  -->
This allows mapping of what is important to what is in scope.

- Who are the intended consumers of this project? Note the different roles that might be involved in the "input" as well as the "output".

- Will new roles need to be developed for this project?

- If new data is being created, there needs to be documentation of requirements for this new data "at rest" and "in transit".

- What data will my project read or write?

- Where does that data come from?

- Will the data be altered in some fashion that could impact other processes that read the data, either by changing its location, altering (or adding) security measures, or transforming the data into a different format?

- Does any new data created by the change need labeling, tagging or some other addition/configuration that if not in place could result in exposure, compromise, or some other risk?

- Based upon previous incidents and past risks, create a list of areas or the types of risks that are most likely to occur.


## Stage IV - Threat Analysis
<!--
 * Probabilistic attack scenario analysis, where any scenario that could happen is at least listed.
 * Regression analysis on security events, where we example events that touch on some of the same or similar components.
 * Threat intel correlation, by taking data from logs, Hackerone reports, incidents, and other sources that can be used to indicate an attack scenario.
  -->
This is where we mainly look to document relevant threats and threat patterns to the data we have gathered up until now.

- Irregardless of possibility or probability, what are the potential threats created with this project?

- Are there base threats we need to consider?

- Are there other systems or components that if flawed could introduce risk to our project (and vice versa)?

- Are there known threats to us that could pose a potential immediate threat to our project once implemented?

- Is there anything about the implementation or creation process itself that could introduce risk?

Go through the example risks, and examine each one for possibilities not considered:
   * System compromised or crashed
   * Service compromised or inoperable
   * Authentication bypassed, breached, or rendered inoperable
   * Data exposed, altered, relocated, or deleted

## Stage V - Vulnerability and weakness analysis
<!--
 * Examine existing vulnerability reports and issues tracking.
 * Threat to existing vulnerability mapping using threat trees.
 * Design Flaw analysis using use and abuse cases.
 * Scorings (CVSS) and Enumeration (CWE/CVE).
 * Impacted systems, sub-systems, data. Are we adding to or altering something that has a history of exploitation. Is the current state vulnerable, and will this change potentially be influenced by or change that assessment.
  -->
This is where we examine the threats, ranking them and assessing their likelihood.

- Develop a detailed list of past incidents that involve the same or similar systems/processes/code base. The list should include mitigations to correct the issues, including the time to completion and complexity of the mitigation. The list should also include whether the mitigation could be implemented quicker now (using the past experience to guide through potential trouble spots).

- Compare and contrast the past incidents with the new list, and use this as a ranking for how likely similar past incidents are likely to occur.

- If possible use Attack Tree modeling to develop threat trees with the vulnerabilities.

- Design flaw analysis of the project, to try and link theoretical flaws to existing project.

- As the threat list moves forward, consider CVSS/CWE mappings, and compare those mappings to post incidents/past flaws found.

## Stage VI - Attack modeling
<!--
 * Attack surface analysis for the impacted components.
 * Attack tree development. This is where something like MITRE ATT&CK can assist.
 * Attack --> Vulnerability --> Exploit analysis using attack trees.
 * Summarize the impact and explain each risk.
  -->
We go through the threats and turn them into attacks, by examining the attack surface before and after our proposed changes.

- Make a list of potential threat actors, using the previous gathered data for assistance, list the perceived attack methods each would use against assets associated with this change, and rank the list of attack methods each threat actor would use by likelihood/preferred/document methods of choice

- Make a list of the existing and changed attack surface "points" on the project's "map"

- Align past and theoretical threats to the attack surface.

- Each risk should be run through an attack tree.

- Create an impact assessment and explain each risk in terms of possible vs impossible

## Stage VII - Risk and Impact Analysis
<!--
 * Qualify and quantify the business impact.
 * Countermeasure identification and residual impact.
 * Identify risk mitigation strategies. Effectiveness of mitigation(s) vs cost to implement mitigation.
 * Residual benefits, as the implementation of a change to single component as a mitigation effort could mean increased security for other systems that access that component.
-->
This covers the development of the rationale for mitigation based upon residual risk.

- Create a list of business impacts from the various scenarios outlined in the previous stages.

- Countermeasures for each risk need to be determined

- From the mitigations, rank them as to how easily or hard it could be to mitigate the risk, and bear in mind that an easy-to-implement mitigation that solves a highly unlikely risk scenario should could still be implemented, improving security overall

- Document mitigations that will be applied to address the risks, based upon likelihood, cost to business, etc, but consider residual benefits of some mitigations if a hard and costly mitigation will improve the security of additional systems/subsystems/processes etc

<!--
The following three sections are entirely optional, but might be useful in helping out the overall process. Uncomment and fill out as you see fit.
  -->

<!--
### Blind threat model.

GitLab’s best practices applied to components of the project.

 * Maps key goals of app or service and correlates to clear technical standards for architecture, hardening of server/service, app framework, containers, etc.
 * Best practices per component. For example, TLS settings that are set to a GitLab standard, noting if our own standard is higher or lower than industry best practices.
 * Best practices for coding are applied here as well, the “Sec” part of DevSecOps and our integration of this into CI/CD.
 * SAST/DAST policies and scopes. We can "eat our own dogfood" to improve the quality of the changes we implement.

Apply to Stage I & Stage II above.
  -->
<!--
### Evidence Driven threat model.

Proof of a threat via numerous indicators as opposed to just theory or conjecture.

 * Integrate threat log data analysis.
 * Focus on logs that support attack vector with the greatest motives (e.g. TLS MITM vs Injection-based attacks).
 * Correlate threat intel for foreseeing trends of attacks for target applications that are related to project components.

Apply to Stage III, IV, V above.
  -->
<!-->
### Full Risk Based Threat Model

 * Statistical/probabilistic analysis on threat data & attack effectiveness
 * Consider non-traditional threat vectors
 * This includes threat intel, H1 trends, existing logs
 * Substantiate the threats that are defined with real data

Apply to all stages above.
  -->
<!-- 
During the initial uses of this template, I'd like to monitor how they are working, so unless there is a privacy reason not to include me, please leave the cc in for my own benefit for now. This will eventually be removed later.

Major changes to the framework please submit the changes in the handbook!
  -->
/cc @mloveless
