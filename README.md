# Threat Modeling Template

This project will house the template(s) used for threat modeling within GitLab.

The current full template is [here](main-template.md), and the main threat model template is located [here](threat_model.md).
There's also a [short template](short-template.md) available.
